#include <ros/ros.h>

#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Trigger.h>
#include <pluginlib/class_loader.h>
#include <realtime_tools/realtime_publisher.h>
#include <coe_driver/CoeMessage.h>
#include <coe_driver/CoeErrors.h>
#include <coe_driver/Ds402States.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>

#include <coe_hw_plugins/coe_ds402_plugin.h>

class TestPlugin
{
private:
  
  
  uint16_t* status_word;
  uint16_t* control_word;
  double* pos_msr, *vel_msr, *eff_msr;
  double* pos_cmd, *vel_cmd, *eff_cmd;
  double* stepper_vel_cmd;
  double* stepper_vel_pos;
  double* stepper_vel_eff;
  std::vector<double*>  ai_msr;
  std::vector<bool*>    di_msr;
  std::vector<bool*>    do_msr;
  
  pluginlib::ClassLoader<coe_driver::CoeHwPlugin>   loader_;
  boost::shared_ptr<coe_hw_plugins::DS402Complete>  DS402plugin_;
  boost::shared_ptr<coe_driver::CoeHwPlugin>        AIplugin_;
  boost::shared_ptr<coe_driver::CoeHwPlugin>        DIplugin_;
  boost::shared_ptr<coe_driver::CoeHwPlugin>        DOplugin_;
  boost::shared_ptr<coe_driver::CoeHwPlugin>        STMplugin_;

  bool  active_DS402plugin_;
  bool  active_AIplugin_;
  bool  active_DIplugin_;
  bool  active_DOplugin_;
  bool  active_STMplugin_;

  std::shared_ptr< realtime_tools::RealtimePublisher< std_msgs::Int8MultiArray    > > di_pub_; 
  std::shared_ptr< realtime_tools::RealtimePublisher< std_msgs::Float64MultiArray > > ai_pub_; 
  std::shared_ptr< realtime_tools::RealtimePublisher< coe_driver::Ds402States     > > state_pub_; 
  
public:
  TestPlugin( ros::NodeHandle& nh ) 
    : loader_( "coe_hw_plugins", "coe_driver::CoeHwPlugin" ), ai_msr(6),di_msr(6,NULL), do_msr(6,NULL)
    , active_DS402plugin_(false)
    , active_AIplugin_   (false)
    , active_DIplugin_   (false)
    , active_DOplugin_   (false)
    , active_STMplugin_  (false)
  {
    try
    {
     
      
        boost::shared_ptr<coe_driver::CoeHwPlugin> DS402plugin;
        DS402plugin = loader_.createInstance ( "coe_hw_plugins::DS402Complete" );
        
        DS402plugin_.reset( static_cast<coe_hw_plugins::DS402Complete*>( DS402plugin.get() ) );
        if( DS402plugin_->initialize ( nh, "/coe_driver_node/coe/", 1) )
        {
          status_word  = DS402plugin_->stateHandle();
          control_word = DS402plugin_->controlWordHandle();
          DS402plugin_->jointStateHandle  (&pos_msr, &vel_msr, &eff_msr);
          DS402plugin_->jointCommandHandle(&pos_cmd, &vel_cmd, &eff_cmd);
          active_DS402plugin_ = true;
        }
        
        //---
        AIplugin_   = loader_.createInstance ( "coe_hw_plugins::AICompact" );
        if( AIplugin_->initialize ( nh, "/coe_driver_node/coe/", 5) )
        { 
          for( size_t i=0; i< ai_msr.size(); i++ )
          {
            ai_msr[ i ] = AIplugin_->analogInputValueHandle( "AI"+ std::to_string(i+1));
          }
          active_AIplugin_ = true;
        }
        
        //---
        DIplugin_   = loader_.createInstance ( "coe_hw_plugins::DICompact" );
        if( DIplugin_->initialize ( nh, "/coe_driver_node/coe/", 3 ) )
        {
          for( size_t i=0; i<di_msr.size(); i++ )
          {
            di_msr[ i ] = DIplugin_->digitalInputValueHandle( "DI"+ std::to_string(i+1) );
          }
          active_DIplugin_ = true;
        }

        //---
        DOplugin_   = loader_.createInstance ( "coe_hw_plugins::DOCompact" );
        if( DOplugin_->initialize ( nh, "/coe_driver_node/coe/", 4 ) )
        {
          for( size_t i=0; i<=do_msr.size(); i++ )
          {
            do_msr[ i ] = DOplugin_->digitalOutputValueHandle( "DO"+ std::to_string(i+1));
          }
          active_DOplugin_ = true;
        }
        
        STMplugin_ = loader_.createInstance ( "coe_hw_plugins::StepperMotor" );
        if( STMplugin_->initialize ( nh, "/coe_driver_node/coe/", 1) )
        {
          STMplugin_->jointCommandHandle( &stepper_vel_pos, &stepper_vel_cmd, &stepper_vel_eff);
          active_STMplugin_ = true;
        }
        
        di_pub_.reset( new realtime_tools::RealtimePublisher< std_msgs::Int8MultiArray >(nh, "get_di", 4) );
       
        ai_pub_.reset( new realtime_tools::RealtimePublisher< std_msgs::Float64MultiArray >(nh, "get_ai", 4) );
        
        state_pub_.reset( new realtime_tools::RealtimePublisher< coe_driver::Ds402States >(nh, "get_ds402device_states", 4) );
  
    }
    catch ( pluginlib::PluginlibException& ex )
    {
        ROS_ERROR ( "The plugin failed to load for some reason. Error: %s", ex.what() );
    }

  }
  ~TestPlugin() 
  {
  }
  
  bool getCoeState( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
  {
    std::string moo_str = DS402plugin_->getActualState( );
    coe_core::ds402::ModeOperationID  moo = coe_core::ds402::to_modeofoperationid( moo_str );
    res.message  += "Mode of Operation Active: " + std::to_string( moo ) + " - " + coe_core::ds402::to_string(moo) + "\n";
    if( moo == coe_core::ds402::MOO_HOMING )
    {
      coe_core::ds402::homing_status_word_t status_word; 
      res.message  += coe_core::ds402::to_string(status_word << DS402plugin_->getStatusWord(), 's' );
      
      coe_core::ds402::homing_control_word_t control_word; 
      res.message  += coe_core::ds402::to_string(control_word << DS402plugin_->getControlWord(), 's' );
    }
    else if( moo == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION )
    {
      coe_core::ds402::cyclic_pos_status_word_t status_word; 
      res.message  += coe_core::ds402::to_string(status_word << DS402plugin_->getStatusWord(), 's' );
      
      coe_core::ds402::cyclic_pos_control_word_t control_word = DS402plugin_->getControlWord(); 
      res.message  += coe_core::ds402::to_string(control_word, 's' );
    } 
    else if( moo == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY )
    {
      coe_core::ds402::cyclic_pos_status_word_t status_word; 
      res.message  += coe_core::ds402::to_string(status_word << DS402plugin_->getStatusWord(), 's' );
      
      coe_core::ds402::cyclic_pos_control_word_t control_word = DS402plugin_->getControlWord();
      res.message  += coe_core::ds402::to_string(control_word, 's' );
      
    }
    else
    {
      coe_core::ds402::status_word_t status_word = DS402plugin_->getStatusWord();
      res.message  += coe_core::ds402::to_string(status_word, 's' );
      
      coe_core::ds402::control_word_t control_word = DS402plugin_->getControlWord(); 
      res.message  += coe_core::ds402::to_string(control_word, 's' );
    }

    std::cout << res.message << std::endl;
    return (res.success = true);
  }
  
  bool setCoeCommand ( coe_driver::CoeMessage::Request& req, coe_driver::CoeMessage::Response& res )
  {
  //
    ROS_INFO ( "%s<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s",BOLDCYAN(),RESET());
    ROS_INFO ( "Received the Coe Command '%s', module address: %d", req.message.c_str(), req.addr );
    coe_core::ds402::CommandID      cmd;
    coe_core::ds402::status_word_t  status_word = DS402plugin_->getStatusWord();
    coe_core::ds402::StateID        start_state  = coe_core::ds402::to_stateid ( status_word );
    std::cout << "Actual Status: '"<< BOLDYELLOW() << coe_core::ds402::to_string(start_state) << RESET() << "'" << std::endl;
    try
    {
      cmd = coe_core::ds402::to_commandid ( req.message );
      std::cout << "Parsed comand: '" << BOLDYELLOW() << coe_core::ds402::to_string( cmd) << RESET() << std::endl;
    }
    catch ( std::exception& e )
    {
      res.message += "Exception:" + std::string ( e.what() );
      res.message += "----\n";
      res.message += "Req. Command: " + std::string ( req.message ) + "\n";
      res.message +=  coe_core::ds402::echo_feasible_commands ( start_state );
      res.success = false;
      return true;
    }

    try
    {

      if(( coe_core::ds402::get_next_state ( start_state, cmd ) == start_state )
      && ( cmd != coe_core::ds402::CMD_LOOPBACK ) && ( cmd != coe_core::ds402::CMD_SWITCH_ON ) && ( cmd != coe_core::ds402::CMD_COMPLETE_RESET ) )
      {
          res.message  = "Error, the command '" + req.message +"' cannot be applied\n" + coe_core::ds402::echo_feasible_commands ( start_state );
          res.success = true;
      }
      else
      {
        coe_core::ds402::StateID next_state = coe_core::ds402::get_next_state ( start_state, cmd );
        std::cout << "Next Status:   '" << BOLDYELLOW() << coe_core::ds402::to_string(next_state) << RESET() << std::endl;
        
        coe_core::ds402::control_word_t control_word;
        coe_core::ds402::to_control_word( cmd, (uint16_t*)&control_word );
        std::cout << coe_core::ds402::to_string(control_word,'s') << std::endl;
        DS402plugin_->setControlWord( control_word );

        res.message  = "None";
        res.success = true;
        
        ROS_WARN ( "*** Wait for the state transition.." );
        coe_core::ds402::StateID act_state; 
        ros::Rate r(10);
        do 
        {
          r.sleep();
          act_state = coe_core::ds402::to_stateid ( DS402plugin_->getStatusWord() );
          if( act_state != start_state )
          {
            std::cout << "New Status:   '" << BOLDYELLOW() << coe_core::ds402::to_string(next_state) << RESET() << std::endl;
            std::cout << "what_is_happen: " <<   coe_core::ds402::what_is_happen( start_state, act_state, 1);
          }
          if(  act_state == next_state )
            break;;
        }
        while( 1 );
        std::cout << coe_core::ds402::echo_feasible_commands ( act_state  ) <<std::endl;
      }

      ROS_INFO ( "%s>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s",BOLDCYAN(),RESET());
      return true;
    }
    catch ( std::exception& e )
    {
      res.message  += "Req. Command: " + std::string ( req.message.data() );
      res.message  += "Exception caught :" + std::string ( e.what() );
      res.success = false;
      return true;
    }
  }
  
  bool setOperationMode ( coe_driver::CoeMessage::Request& req, coe_driver::CoeMessage::Response& res )
  {
  //
    ROS_INFO ( "Received the Coe Command '%s%s%s', module address: %s%d%s", BOLDYELLOW(),req.message.c_str(), RESET(), BOLDYELLOW(), req.addr, RESET() );
    std::string                       actual_operation_mode = DS402plugin_->getActualState();
    coe_core::ds402::ModeOperationID  actual_moo = coe_core::ds402::to_modeofoperationid( actual_operation_mode );
    ROS_INFO ( "Actual moo '%s%s%s', asked '%s%s%s', module address: %d", BOLDYELLOW(),actual_operation_mode.c_str(), RESET(), BOLDYELLOW(),req.message.c_str(), RESET(), req.addr );
    
    auto ns = DS402plugin_->getStateNames();
    
    if( std::find(ns.begin(), ns.end(), req.message) == ns.end() )
    {
      ROS_ERROR ( "Asked mode not available.");
      ROS_INFO  ( "Available modes:");
      for( auto const & s : ns )
        ROS_INFO ( "- %s", s.c_str());
      return false;
    }
    
    try
    {
      res.success = DS402plugin_->setTargetState( req.message ); 
    }
    catch ( std::exception& e )
    {
        res.message += "Exception:" + std::string ( e.what() );
        res.success = false;
        return true;
    }
    return true;
  }
  
  
  bool setStepperOperationMode ( coe_driver::CoeMessage::Request& req, coe_driver::CoeMessage::Response& res )
  {
  //
    ROS_INFO ( "Received the Steper Command '%s%s%s', module address: %s%d%s", BOLDYELLOW(),req.message.c_str(), RESET(), BOLDYELLOW(), req.addr, RESET() );
    
    auto ns = STMplugin_->getStateNames();
    
    if( std::find(ns.begin(), ns.end(), req.message) == ns.end() )
    {
      ROS_ERROR ( "Asked mode not available.");
      ROS_INFO  ( "Available modes:");
      for( auto const & s : ns )
        ROS_INFO ( "- %s", s.c_str());
      return false;
    }
    
    try
    {
      ROS_INFO ( "Set State '%s%s%s', module address: %s%d%s", BOLDYELLOW(),req.message.c_str(), RESET(), BOLDYELLOW(), req.addr, RESET() );
      res.success = STMplugin_->setTargetState( req.message ); 
    }
    catch ( std::exception& e )
    {
        res.message += "Exception:" + std::string ( e.what() );
        res.success = false;
        return true;
    }
    return true;
  }
  
  
  bool getOperationMode ( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
  {
    res.message = DS402plugin_->getActualState();
    res.success = true; 
    return true;
  }
  
  void setCoeTargetPosition(const std_msgs::Float64::ConstPtr& msg)
  {
    *pos_cmd = msg->data;
    *vel_cmd = 0.0;
    *eff_cmd = 0.0;
  }
  void setDigitalOutput(const std_msgs::Int8MultiArray::ConstPtr& msg)
  {
    for( size_t i=0; i <std::min( msg->data.size(),do_msr.size() ); i++ )
    {
      *(do_msr[i]) = msg->data[i];
    }
  }
  
  
  void setCoeTargetVelocity(const std_msgs::Float64::ConstPtr& msg)
  {
    *pos_cmd = *pos_msr;
    *vel_cmd = msg->data;
    *eff_cmd = 0.0;
  }
  void setStepperTargetVelocity(const std_msgs::Float64::ConstPtr& msg)
  {
    *stepper_vel_cmd = msg->data;
  }
  void setCoeTargetTorque(const std_msgs::Float64::ConstPtr& msg)
  {
    *pos_cmd = *pos_msr;
    *vel_cmd = 0.0;
    *eff_cmd = msg->data;
  }



  bool sync()
  {
    bool ret = true;
    if( active_DS402plugin_ )
    {
      ret &= DS402plugin_->areErrorsActive();
      
      if( state_pub_ && state_pub_->trylock() )
      {
        state_pub_->msg_.addresses.clear();
        state_pub_->msg_.statesid.clear();
        state_pub_->msg_.states.clear();
        state_pub_->msg_.load_position.clear();
        state_pub_->msg_.load_velocity.clear();
        state_pub_->msg_.load_effort.clear();

        state_pub_->msg_.addresses.push_back    ( 0 );
        coe_core::ds402::StateID sid = coe_core::ds402::to_stateid( *status_word );
        state_pub_->msg_.statesid.push_back  (  coe_core::ds402::to_string( sid ) );
        
        state_pub_->msg_.states.push_back( sid );
        state_pub_->msg_.load_position.push_back( *pos_msr );
        state_pub_->msg_.load_velocity.push_back( *vel_msr );
        state_pub_->msg_.load_effort.push_back  ( *eff_msr );
        
        state_pub_->unlockAndPublish();
      }    
    }
    
    if( active_AIplugin_ )
    {
      ret &= AIplugin_->areErrorsActive();
      if( ai_pub_ && ai_pub_->trylock() )
      {
        ai_pub_->msg_.data.clear();
        for( auto const & a_i :  ai_msr ) ai_pub_->msg_.data.push_back  ( *a_i );
        ai_pub_->unlockAndPublish();
      }
    }

    if( active_DIplugin_ )
    {
      ret &= DIplugin_->areErrorsActive();
      if( di_pub_ && di_pub_->trylock() )
      {
        di_pub_->msg_.data.clear();
        for( auto const & d_i : di_msr )  di_pub_->msg_.data.push_back  ( *d_i );
        
        di_pub_->unlockAndPublish();
      }
    }

    if( active_DOplugin_ )
    {
      ret &= DOplugin_->areErrorsActive();
    }
    
    if(active_STMplugin_)
    {
      ret &= STMplugin_->areErrorsActive();
    }
    
    return ret;
  }
  
};

int main ( int argc, char* argv[] )
{
    ros::init ( argc,argv,"test_coe_hw_plugins" );
    ros::NodeHandle nh("~");
    ros::AsyncSpinner spinner(4);
    std::string device_coe_parameter = "";

    TestPlugin test( nh );
    
    spinner.start();
    
    ros::ServiceServer coe_command_service_ = nh.advertiseService                     ( "set_coe_command",        &TestPlugin::setCoeCommand,        &test );
    ros::ServiceServer coe_config_service_  = nh.advertiseService                     ( "set_operation_mode",     &TestPlugin::setOperationMode,     &test );
                                                                                                                  
    ros::ServiceServer coe_moo_service_     = nh.advertiseService                     ( "get_operation_mode",     &TestPlugin::getOperationMode,     &test);
    ros::ServiceServer coe_status_service_  = nh.advertiseService                     ( "get_status_word",        &TestPlugin::getCoeState,          &test);
                                                                                                                  
    ros::Subscriber    coe_digout_          = nh.subscribe<std_msgs::Int8MultiArray>  ( "set_do"             , 1, &TestPlugin::setDigitalOutput,     &test);
    ros::Subscriber    coe_target_pos_      = nh.subscribe<std_msgs::Float64>         ( "set_target_position", 1, &TestPlugin::setCoeTargetPosition, &test);
    ros::Subscriber    coe_target_vel_        = nh.subscribe<std_msgs::Float64>       ( "set_target_velocity", 1, &TestPlugin::setCoeTargetVelocity, &test);
    ros::Subscriber    coe_target_tau_        = nh.subscribe<std_msgs::Float64>       ( "set_target_torque",   1, &TestPlugin::setCoeTargetTorque,   &test);
    
    ros::ServiceServer coe_stepper_service_   = nh.advertiseService                   ( "set_stepper_mode"      ,  &TestPlugin::setStepperOperationMode,  &test );
    ros::Subscriber    coe_stepper_target_vel_= nh.subscribe<std_msgs::Float64>       ( "set_stepper_velocity", 1, &TestPlugin::setStepperTargetVelocity, &test);

    ros::WallRate rt(1000);
    while(ros::ok())
    {
      if( !test.sync() )
        break;
      
      rt.sleep();
    }
    
    ROS_INFO("Exit from CoE Plugin Test. Bye bye.");

    return -1;
}


