#include <realtime_tools/realtime_publisher.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>
#include <coe_driver/Ds402States.h>
 
#include <coe_core/coe_base.h>
#include <coe_core/ds301/coe_bitwise_struct.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>
#include <coe_core/ds402/coe_bitwise_struct.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>

#include <coe_driver/diagnostic/coe_generic_analyzer.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>
#include <coe_hw_plugins/coe_ds402_plugin.h>


namespace coe_hw_plugins
{

DS402Complete::DS402Complete( ) 
: switching_operation_mode_ ( false )
, statusid_prev_            ( coe_core::ds402::STATE_NO_STATE )
, operation_mode_           ( coe_core::ds402::MOO_NO_MODE )
{ 


}

DS402Complete::~DS402Complete( ) 
{ 
  if( state_pub_ ) 
  {
    state_pub_->stop(); 
  }

  if( command_pub_ ) 
  {
    command_pub_->stop(); 
  }
  ROS_DEBUG("DS402Complete Destroyed");
  ROS_DEBUG("Destroy base class");
  // this->~CoeHwPlugin();
}




///////////////////////////////////////
bool DS402Complete::initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address)
{
  if (!CoeHwPlugin::initialize(nh, device_coe_parameter, address) )
    return false;
  
  auto available_moo = coe_core::ds402::SUPPORTED_DRIVE_MODES();
  if(!readSdo(&available_moo) )
  {
    ROS_ERROR("Impossible to get the supported drive modes");
    return false;
  }
  std::bitset< 32 > smoo( available_moo.value() );
  state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_NO_MODE) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_PROFILED_POSITION                                ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_PROFILED_POSITION              ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_OPEN_LOOP_VELOCITY                               ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_OPEN_LOOP_VELOCITY             ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_PROFILED_VELOCITY                                ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_PROFILED_VELOCITY              ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_PROFILED_TORQUE                                  ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_PROFILED_TORQUE                ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_RESERVED                                         ] ) ;                                                                                                                         
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_HOMING                                           ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_HOMING                         ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_INTERPOLATED_POSITION                            ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_INTERPOLATED_POSITION          ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_POSITION                      ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION    ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_VELOCITY                      ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY    ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_TORQUE                        ] ) state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE      ) );
  if( smoo [ coe_core::ds402::SupportedModeOperationID::SMOO_CYCLIC_SYNCHRONOUS_TORQUE_WITH_COMMUTATION_ANGLE ] ) ;

  //////////////////////////////////////////////////////////////////
  {
    ROS_DEBUG("[ %s%s%s ] Check the Tx mandatory objects [%sRUNNING%s]",BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), BOLDMAGENTA(),RESET());
    std::vector< std::string > key( std::begin( TxPdoMandatory::Key   ), std::end(TxPdoMandatory::Key) );
    std::vector< uint32_t >    idx( std::begin( TxPdoMandatory::Index ), std::end(TxPdoMandatory::Index) );
    for( size_t j=0; j<key.size(); j++ )
    {
      auto const & idx_ = idx[j];
      auto const & key_ = key[j];

      if( module_->getTxPdo ( ).find( idx_ ) == module_->getTxPdo( ).end() )
      {
        ROS_ERROR("[ %s%s%s ] The field '%s/%x' is not in the PDO list", BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), key_.c_str(),  idx_);
        ROS_INFO ("[ %s%s%s ] PDOs mapped: \n%s"                       , BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), module_->getTxPdo ( ).to_string().c_str() );
        ROS_ERROR("[ %s%s%s ] Abort."                                  , BOLDCYAN(), module_->getIdentifier().c_str(), RESET() );
        return false;
      }
    }
    ROS_DEBUG_STREAM( module_->getTxPdo ( ).to_string() );
    ROS_DEBUG("[ %s%s%s ] Check the Tx mandatory objects [%sOK%s]",BOLDCYAN(),module_->getIdentifier().c_str(), RESET(), BOLDGREEN(), RESET()); 
  }

  {
    ROS_DEBUG("[ %s%s%s ] Check the Rx mandatory objects [%sRUNNING%s]",BOLDCYAN(), module_->getIdentifier().c_str(), RESET(),BOLDMAGENTA(),RESET());
    std::vector< std::string > key( std::begin( RxPdoMandatory::Key ), std::end( RxPdoMandatory::Key ) );
    std::vector< uint32_t >    idx( std::begin( RxPdoMandatory::Index ), std::end( RxPdoMandatory::Index ) );
    for( size_t j=0; j<key.size(); j++ )
    {
      auto const & idx_ = idx[j];
      auto const & key_ = key[j];
      if( module_->getRxPdo ( ).find( idx_ ) == module_->getRxPdo ( ).end() )
      {
        ROS_ERROR("[ %s%s%s ] The field '%s/%x' is not in the PDO list" ,BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), key_.c_str(),  idx_);        
        ROS_INFO ("[ %s%s%s ] PDOs mapped: \n%s"                        ,BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), module_->getRxPdo ( ).to_string().c_str() );
        ROS_ERROR("[ %s%s%s ] Abort."                                   ,BOLDCYAN(), module_->getIdentifier().c_str(), RESET());
        return false;
      }
    }
    ROS_DEBUG_STREAM( module_->getRxPdo ( ).to_string() );
    ROS_DEBUG("[ %s%s%s ] Check the Rx mandatory objects [%sOK%s]",BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), BOLDGREEN(),RESET()); 
  }
  
  //Pointer to 
  //ROS_DEBUG("[ %s%s%s ] has analog inputs? %s",BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), ( module_->getAnalogInputs( ).size() ? "YES" : "NO" ) );
  for( auto const & a_i : module_->getAnalogInputs  ( ) ) analog_inputs_  [ a_i.first ] = 0.0;
  
  //ROS_DEBUG("[ %s%s%s ] has analog outputs? %s",BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), ( module_->getAnalogOutputs( ).size() ? "YES" : "NO" ) );
  for( auto const & a_o : module_->getAnalogOutputs ( ) ) analog_outputs_ [ a_o.first ] = 0.0;
  
  //ROS_DEBUG("[ %s%s%s ] has digital inputs? %s",BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), ( module_->getDigitalInputs( ).size() ? "YES" : "NO" ) );
  for( auto const & d_i : module_->getDigitalInputs ( ) ) digital_inputs_ [ d_i.first ] = true;
  
  //ROS_DEBUG("[ %s%s%s ] has digital outputs? %s",BOLDCYAN(), module_->getIdentifier().c_str(), RESET(), ( module_->getDigitalOutputs( ).size() ? "YES" : "NO" ) );
  for( auto const & d_o : module_->getDigitalOutputs( ) ) digital_outputs_[ d_o.first ] = false;

 
  statusid_prev_ = coe_core::ds402::STATE_NO_STATE;
  status_word_  = static_cast< coe_core::DataObjectEntry<uint16_t>* >( ( *(module_->getTxPdo().find( TxPdoMandatory::Index[ TxPdoMandatory::STATUS_WORD  ] ) ) ) .get()  ) ;
  control_word_ = static_cast< coe_core::DataObjectEntry<uint16_t>* >( ( *(module_->getRxPdo().find( RxPdoMandatory::Index[ RxPdoMandatory::CONTROL_WORD ] ) ) ) .get()  );

  // Init values
  read();     // assigend:   feedback_position_, feedback_velocity_  = 0.0, feedback_torque_    = 0.0, 
  
  command_position_     = feedback_position_;
  command_position_prev_= command_position_ ;
  command_velocity_     = 0.0;
  command_velocity_prev_= command_velocity_ ;
  command_effort_       = 0.0;
  command_effort_prev_  = command_effort_;
  setControlWord( coe_core::ds402::to_control_word(0x0) );

  clock_gettime(CLOCK_MONOTONIC, &write_time_);

  write_time_prev_ = write_time_;

  coe_core::DataObjectEntry<int32_t > min_position_range_limit      = coe_core::ds402::MIN_POSITION_RANGE_LIMIT   ( );
  coe_core::DataObjectEntry<int32_t > max_position_range_limit      = coe_core::ds402::MAX_POSITION_RANGE_LIMIT   ( );
  coe_core::DataObjectEntry<int32_t > min_software_position_limit   = coe_core::ds402::MIN_SOFTWARE_POSITION_LIMIT( );
  coe_core::DataObjectEntry<int32_t > max_software_position_limit   = coe_core::ds402::MAX_SOFTWARE_POSITION_LIMIT( );
  coe_core::DataObjectEntry<uint32_t> maximum_profile_velocity      = coe_core::ds402::MAXIMUM_PROFILE_VELOCITY   ( );
  coe_core::DataObjectEntry<uint32_t> profiled_acceleration         = coe_core::ds402::PROFILED_ACCELERATION      ( );

  if(!readSdo(&min_position_range_limit    ) ) { ROS_ERROR("Impossible to get the min_position_range_limit_   "); throw std::runtime_error("DS402Complete() error in reading MIN_POSITION_RANGE_LIMIT     SDO"); } 
  if(!readSdo(&max_position_range_limit    ) ) { ROS_ERROR("Impossible to get the max_position_range_limit_   "); throw std::runtime_error("DS402Complete() error in reading MAX_POSITION_RANGE_LIMIT     SDO"); } 
  if(!readSdo(&min_software_position_limit ) ) { ROS_ERROR("Impossible to get the min_software_position_limit_"); throw std::runtime_error("DS402Complete() error in reading MIN_SOFTWARE_POSITION_LIMIT  SDO"); } 
  if(!readSdo(&max_software_position_limit ) ) { ROS_ERROR("Impossible to get the max_software_position_limit_"); throw std::runtime_error("DS402Complete() error in reading MAX_SOFTWARE_POSITION_LIMIT  SDO"); } 
  if(!readSdo(&maximum_profile_velocity    ) ) { ROS_ERROR("Impossible to get the maximum_profile_velocity_   "); throw std::runtime_error("DS402Complete() error in reading MAXIMUM_PROFILE_VELOCITY     SDO"); } 
  if(!readSdo(&profiled_acceleration       ) ) { ROS_ERROR("Impossible to get the profiled_acceleration_      "); throw std::runtime_error("DS402Complete() error in reading PROFILED_ACCELERATION        SDO"); } 


  auto & position =  module_->getAxisFeedback( TxPdoMandatory::Key[TxPdoMandatory::POSITION ] );
  min_position_range_limit_    = min_position_range_limit   .get<int32_t> ( ) * ( ( 2.0 * M_PI / position.counter_per_motor_round ) / position.gear_ratio );
  max_position_range_limit_    = max_position_range_limit   .get<int32_t> ( ) * ( ( 2.0 * M_PI / position.counter_per_motor_round ) / position.gear_ratio );
  min_software_position_limit_ = min_software_position_limit.get<int32_t> ( ) * ( ( 2.0 * M_PI / position.counter_per_motor_round ) / position.gear_ratio );
  max_software_position_limit_ = max_software_position_limit.get<int32_t> ( ) * ( ( 2.0 * M_PI / position.counter_per_motor_round ) / position.gear_ratio );
  maximum_profile_velocity_    = maximum_profile_velocity   .get<uint32_t>( ) * ( ( 2.0 * M_PI / position.counter_per_motor_round ) / position.gear_ratio );
  profiled_acceleration_       = profiled_acceleration      .get<uint32_t>( ) * ( ( 2.0 * M_PI / position.counter_per_motor_round ) / position.gear_ratio );

//   ROS_INFO_STREAM( "MIN_POSITION_RANGE_LIMIT    Value is: " << min_position_range_limit_    );
//   ROS_INFO_STREAM( "MAX_POSITION_RANGE_LIMIT    Value is: " << max_position_range_limit_    );
//   ROS_INFO_STREAM( "MIN_SOFTWARE_POSITION_LIMIT Value is: " << min_software_position_limit_ );
//   ROS_INFO_STREAM( "MAX_SOFTWARE_POSITION_LIMIT Value is: " << max_software_position_limit_ );
//   ROS_INFO_STREAM( "MAXIMUM_PROFILE_VELOCITY    Value is: " << maximum_profile_velocity_    );
//   ROS_INFO_STREAM( "PROFILED_ACCELERATION       Value is: " << profiled_acceleration_       );

  state_pub_.reset();
  std::string ns = device_coe_parameter + "/" + module_->getIdentifier() + "/";
  if( !nh.getParam( ns + "/state_pub_dec", state_pub_dec_) )
  {
      ROS_WARN("The param '%s/state_pub_dec' has not been found in ros param server.  State Publishing disabled", (nh.getNamespace() + ns ).c_str() );
      state_pub_dec_ = -1;
  }
  if( state_pub_dec_ == -1 )
  {
      ROS_WARN("The param '%s/state_pub_dec' is equal tp -1. State Publishing disabled", (nh.getNamespace() + ns ).c_str() );
  }
  else
  {
      ROS_DEBUG("[ %s%s%s ] Crate the state publisher ",BOLDCYAN(), module_->getIdentifier().c_str(), RESET() );
      state_pub_.reset( new realtime_tools::RealtimePublisher< coe_driver::Ds402States > (nh_, module_->getIdentifier()+"_state", 4) );
      state_pub_dec_cnt_ = 0;
  }


  command_pub_.reset();
  if( !nh.getParam(ns + "/command_pub_dec", command_pub_dec_) )
  {
      ROS_WARN("The param '%s/command_pub_dec' has not been found in ros param server.  Cmd Publishing disabled", (nh.getNamespace() + ns).c_str() );
      command_pub_dec_ = -1;
  }
  if( command_pub_dec_ == -1 )
  {
      ROS_WARN("The param '%s/state_command_pub_dec' is equal tp -1. Cmd Publishing disabled", (nh.getNamespace() + ns ).c_str() );
  }
  else
  {
      ROS_DEBUG("[ %s%s%s ] Crate the command publisher ",BOLDCYAN(), module_->getIdentifier().c_str(), RESET() );
      command_pub_.reset( new realtime_tools::RealtimePublisher< coe_driver::Ds402Command > (nh_, module_->getIdentifier()+"_command", 4) );
      command_pub_dec_cnt_ = 0;
  }

  do_control_word_override_ = false;
  control_word_override_ << 0x0;

  return true;
}

coe_core::ds402::ModeOperationID  DS402Complete::getOperationMode ( ) 
{
  auto sdo = coe_core::ds402::MODES_OF_OPERATION_DISPLAY( );
  bool res = readSdo( &sdo );
  if ( !res ) 
  {
    throw std::runtime_error( "Error in gettin the MODES_OF_OPERATION_DISPLAY"  );
  }
  return coe_core::ds402::to_modeofoperationid( sdo.value() ) ;
}

bool DS402Complete::setOperationMode(const coe_core::ds402::ModeOperationID moo, const uint8_t max_num_trials)
{
  ros::WallRate rt( 1. / ( module_->getLoopRateDecimation() * operational_time_ ) );
  
  ros::WallDuration(0.1).sleep();

  uint8_t trial = 0;
  ROS_WARN("[ %s ] Change the DS402 State to STATE_READY_TO_SWITCH_ON", module_->getIdentifier().c_str());

  do
  {    
    coe_core::ds402::StateID        actl_state    = coe_core::ds402::to_stateid ( getStatusWord() );
    ROS_DEBUG("[ %s ] Actual State: %s", module_->getIdentifier().c_str(), coe_core::ds402::to_string( actl_state ).c_str() );
    control_word_override_  = getControlWord();
    switch( actl_state )
    {
      case coe_core::ds402::STATE_FAULT             : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_FAULT_RESET, (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_SWITCH_ON_DISABLED: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SHUTDOWN,    (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_READY_TO_SWITCH_ON: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_LOOPBACK,    (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_SWITCHED_ON       : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SHUTDOWN,    (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_OPERATION_ENABLED : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SHUTDOWN,    (uint16_t*)&control_word_override_); break;
      default:  control_word_override_ << 0x0;
      break;
    }
    ROS_DEBUG("[ %s ] Actual Command: %s", module_->getIdentifier().c_str(), coe_core::ds402::to_string( coe_core::ds402::to_commandid( control_word_override_ ) ).c_str() );
    do_control_word_override_ = true;
    setControlWord(control_word_override_);
    
    rt.sleep();
    actl_state  = coe_core::ds402::to_stateid ( getStatusWord() ); 
    if( actl_state == coe_core::ds402::STATE_READY_TO_SWITCH_ON )
    {
      ROS_WARN("[ %s ] Module in STATE_READY_TO_SWITCH_ON", module_->getIdentifier().c_str());
      break;
    } 
    else if( trial++ > max_num_trials )
    {
      ROS_ERROR("[ %s ] Actual State: %s", module_->getIdentifier().c_str(), coe_core::ds402::to_string( actl_state ).c_str() );
      ROS_ERROR("[ %s ] Aborted in disabling, n. trials %d. Try manually.\n",  module_->getIdentifier().c_str(), trial);
      return false;
    }
    
  } while( 1 );
  

  trial = 0;
  ROS_WARN("[ %s ] Change the operational requested mode '%s'", module_->getIdentifier().c_str(), coe_core::ds402::to_string( moo ).c_str() );
  do 
  {
    if ( trial++ > max_num_trials ) 
    {
      ROS_ERROR("[ %s ] Aborted, n. trials %d", module_->getIdentifier().c_str(),  trial );
      return false;
    }

    //TODO put it in a parallel thread!
    auto sdo = coe_core::ds402::MODES_OF_OPERATION( moo );
    if ( !writeSdo( &sdo ) ) 
    {
      ROS_ERROR("[ %s ] Error in setting SDO (set profile velocity mode)\n", module_->getIdentifier().c_str());
      return false;
    }
    rt.sleep();

  } while( (operation_mode_ = getOperationMode( ) ) != moo ) ;
  
  
  ROS_WARN("[ %s ] Change the DS402 State to STATE_OPERATION_ENABLED", module_->getIdentifier().c_str());
  trial = 0;
   do
  {
    coe_core::ds402::StateID actl_state    = coe_core::ds402::to_stateid ( getStatusWord() );
    switch( actl_state )
    {
      case coe_core::ds402::STATE_FAULT             : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_FAULT_RESET, (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_SWITCH_ON_DISABLED: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SHUTDOWN,    (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_READY_TO_SWITCH_ON: coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_SWITCH_ON,   (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_SWITCHED_ON       : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_ENABLE,      (uint16_t*)&control_word_override_); break;
      case coe_core::ds402::STATE_OPERATION_ENABLED : coe_core::ds402::to_control_word(coe_core::ds402::CommandID::CMD_LOOPBACK,    (uint16_t*)&control_word_override_); break;
    }
    setControlWord(control_word_override_);

    rt.sleep();
    actl_state  = coe_core::ds402::to_stateid ( getStatusWord() ); 

    if( actl_state == coe_core::ds402::STATE_OPERATION_ENABLED )
    {
      do_control_word_override_ = false;
      ROS_WARN("[ %s ] Module in STATE_OPERATION_ENABLED", module_->getIdentifier().c_str());
      operation_mode_ = getOperationMode( );
      ROS_WARN_STREAM("[ " << module_->getIdentifier().c_str() << " ] Module operation mode is: " << coe_core::ds402::to_string( operation_mode_ ) );
      break;
    } 
    else if( trial++ > max_num_trials )
    {
      ROS_ERROR("[ %s ] borted in disabling, n. trials %d. Try manually.\n", module_->getIdentifier().c_str(),  trial );
      return false;
    }
  } while( 1 );
  
  return  true;
}


coe_core::ds402::ModeOperationID  DS402Complete::getOperationModeID( ) 
{
  auto sdo = coe_core::ds402::MODES_OF_OPERATION_DISPLAY( );
  if ( !readSdo( &sdo) ) throw std::runtime_error( "Error in gettin the MODES_OF_OPERATION_DISPLAY"  );

  return coe_core::ds402::to_modeofoperationid( sdo.value() );
}



std::vector<std::string>   DS402Complete::getStateNames ( ) const 
{ 
  return state_names_; 
}

bool DS402Complete::checkErrorsActive ( ) 
{ 
  std::lock_guard<std::mutex> lock(read_mtx_);  
  return  ( switching_operation_mode_  )                         ? false
        : ( operation_mode_ == coe_core::ds402::MOO_NO_MODE )    ? false
        : ( statusid_prev_  != coe_core::ds402::STATE_OPERATION_ENABLED);
}
  
std::string  DS402Complete::getActualState ( )  
{ 
  return coe_core::ds402::MODEOPERATIONSID_STRINGS.at( operation_mode_  ); 
}

coe_core::ds402::status_word_t  DS402Complete::getStatusWord  ( )  
{ 
  coe_core::ds402::status_word_t ret;
  std::lock_guard<std::mutex> lock(read_mtx_);  
  ret = coe_core::ds402::to_status_word( status_word_->value() ); 
  return ret; 
}

coe_core::ds402::control_word_t  DS402Complete::getControlWord  ( )  
{ 
  coe_core::ds402::control_word_t ret;
  std::lock_guard<std::mutex> lock(read_mtx_);  
  ret = coe_core::ds402::to_control_word( control_word_->value() ); 
  return ret; 
}


std::vector<std::string> DS402Complete::getAnalogInputNames ( ) const 
{ 
  std::vector<std::string> n; 
  if( n.size() != module_->getAnalogInputs().size()  )  
    for( auto const & m : module_->getAnalogInputs()   ) 
      n.push_back(m.first); 
  return n; 
}

std::vector<std::string> DS402Complete::getAnalogOutputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getAnalogOutputs()  ) 
    n.push_back(m.first); 
  return n; 
}

std::vector<std::string>   DS402Complete::getDigitalInputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getAnalogInputs()   ) 
    n.push_back(m.first); 
  return n; 
}
std::vector<std::string>   DS402Complete::getDigitalOutputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getDigitalOutputs() ) 
    n.push_back(m.first); 
  return n; 
}
uint16_t* DS402Complete::stateHandle ( )
{
  return (uint16_t*)status_word_->data();
}
uint16_t* DS402Complete::controlWordHandle( )
{
  return (uint16_t*)control_word_->data();
}

void  DS402Complete::jointStateHandle   (double** pos, double** vel, double** eff) 
{
  *pos = &feedback_position_;
  *vel = &feedback_velocity_;
  *eff = &feedback_torque_;

}
void  DS402Complete::jointCommandHandle (double** pos, double** vel, double** eff)  
{
  *pos = &command_position_;
  *vel = &command_velocity_;
  *eff = &command_effort_;
}

double* DS402Complete::analogInputValueHandle  (const std::string& name) 
{
  double* ret = nullptr;
  try
  {
    ret = &( analog_inputs_[name] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret = nullptr;
  }
  return ret;
}
double*  DS402Complete::analogOutputValueHandle (const std::string& name) 
{
  double* ret = nullptr;
  try
  {
  
  ret = & ( analog_outputs_[ name ] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret = nullptr;
  }
  return ret;
}

bool* DS402Complete::digitalInputValueHandle  (const std::string& name) 
{
  
  bool* ret = nullptr;
  try
  {
    ret = &  ( digital_inputs_[ name ] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret = nullptr;
  }
  return ret;

}
bool*  DS402Complete::digitalOutputValueHandle (const std::string& name)  
{
  bool* ret=nullptr;
  try
  {
    ret = & ( digital_outputs_[ name ] );
  }
  catch(std::exception& e)
  {
    ROS_FATAL("Error in %s: %s", __FUNCTION__, e.what() );
    ret=nullptr;
  }
  return ret;
}

void DS402Complete::getDeviceErrors ( std::vector< std::pair< std::string,std::string> >& ret )
{ 
  ret.clear();

  auto eregister      = coe_core::ds301::ERROR_REGISTER();
  auto emanufacturer  = coe_core::ds301::MANUFACTURER_STATUS_REGISTER( );
  auto epredef        = coe_core::ds301::PREDEFINED_ERROR_FIELD_N();

  if( readSdo( &eregister) ) 
  {
    coe_core::ds301::error_register_t error_register; 
    if( eregister.value() )
      ret.push_back( std::make_pair( coe_core::to_string_hex( eregister.value() ), coe_core::ds301::to_string( error_register << eregister.value() ) ) );
  }

  if( readSdo(&emanufacturer) )
  {
    coe_core::ds301::manufacturer_status_register_t  manufacturer_status_register;
    if( emanufacturer.value() )
      ret.push_back( std::make_pair( coe_core::to_string_hex( emanufacturer.value() ), coe_core::ds301::to_string( manufacturer_status_register << emanufacturer.value() ) ) );
  }

  if( readSdo(&epredef) )
  {
    for( size_t i=1; i<= epredef.value(); i++) 
    {
      uint32_t val = 0;
      coe_core::DataObjectEntry<uint32_t> error_field( 0x1003, i, "Predefined Error Field", val );
      if( readSdo(&error_field) )
      {
          coe_core::ds301::predefined_error_field_t predefined_error_field;
          ret.push_back( std::make_pair( coe_core::to_string_hex(error_field.value()), coe_core::ds301::to_string( predefined_error_field << error_field.value() ) ) );
      }
    }
  }
  
  coe_core::ds402::status_word_t s = getStatusWord();
  ret.push_back( std::make_pair( "Status Word", coe_core::ds402::to_string(s,'s') ) );
}

bool DS402Complete::setTargetState( const std::string& state_name )
{
  bool ret = false;
  switching_operation_mode_ = true;
  coe_core::ds402::ModeOperationID moo = coe_core::ds402::to_modeofoperationid(state_name);
  switch( moo )
  {
    case coe_core::ds402::MOO_NO_MODE                      : control_word_->set<uint16_t>(0x0); ret = true; break;
    case coe_core::ds402::MOO_HOMING                       : ret = setOperationMode( coe_core::ds402::MOO_HOMING );                     break;
    case coe_core::ds402::MOO_PROFILED_POSITION            : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_PROFILED_VELOCITY            : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_PROFILED_TORQUE              : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_INTERPOLATED_POSITION        : throw std::runtime_error(( "'"+ state_name + "' Not yet suported").c_str() );   break;
    case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION  : ret = setOperationMode( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION ); break;
    case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY  : ret = setOperationMode( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY ); break;
    case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE    : ret = setOperationMode( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_TORQUE   ); break;
  }
  switching_operation_mode_ = !ret;
  return ret;
}

bool DS402Complete::setControlWord( const coe_core::ds402::control_word_t& control_word )
{
  ROS_DEBUG("setControlWord (%s)", coe_core::ds402::to_string( control_word, 's' ).c_str( ) );
  (*control_word_) << (uint8_t*)&control_word;
  return true;
}


coe_driver::CoeHwPlugin::Error DS402Complete::read()
{
  coe_core::ds402::StateID stateid = coe_core::ds402::STATE_NO_STATE;
  CoeHwPlugin::Error       ret = CoeHwPlugin::NONE_ERROR;
  try 
  {
    ret = CoeHwPlugin::read();
    if( ret != CoeHwPlugin::NONE_ERROR )
    {
      ROS_FATAL("Error in CoeHwPlugin::read()" );
      return ret;
    }
    
    std::lock_guard<std::mutex> lock(read_mtx_);  
    auto & position =  module_->getAxisFeedback( TxPdoMandatory::Key[TxPdoMandatory::POSITION ] );
    feedback_position_ = position.entry->get<int32_t>( ) * ( ( 2.0 * M_PI / position.counter_per_motor_round ) / position.gear_ratio );
    
    auto & velocity =  module_->getAxisFeedback( TxPdoMandatory::Key[TxPdoMandatory::VELOCITY ]);
    feedback_velocity_ = velocity.entry->get<int32_t>( ) * ( ( 2.0 * M_PI / velocity.counter_per_motor_round ) / velocity.gear_ratio );

    auto & torque =  module_->getAxisFeedback( TxPdoMandatory::Key[TxPdoMandatory::TORQUE] );
    feedback_torque_ = torque.entry->get<int16_t>( ) * ( ( torque.nominal_motor_torque / (1000.0) ) * torque.gear_ratio );

    for( auto & a_i  :  module_->getAnalogInputs(  ) )
    { 
      analog_inputs_[ a_i.first ] = a_i.second.entry->get<int32_t>( ) * a_i.second.scale +  a_i.second.offset;
    }

    for( auto & d_i  :  module_->getDigitalInputs(  ) )
    {
      std::bitset< 32 > bits( d_i.second.entry->get<uint32_t>( ) ); 
      digital_inputs_[ d_i.first ] = bits[ d_i.second.bit ];
    }
    
    stateid = coe_core::ds402::to_stateid( status_word_->value() );
    if( stateid != statusid_prev_ )
    {
      ROS_INFO_STREAM( coe_core::ds402::what_is_happen(statusid_prev_,stateid,1)); 
    }
    statusid_prev_  = stateid;
    
    
    switch( operation_mode_ )
    {
      case coe_core::ds402::MOO_HOMING:
      {
        command_position_ = feedback_position_;
        command_effort_   = 0.0;
        command_velocity_ = 0.0;
      } 
      break;
      case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION:
      case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY:
      {
        if( stateid == coe_core::ds402::STATE_OPERATION_ENABLED) 
        {
          coe_core::ds402::cyclic_pos_status_word_t*  sw_ = (coe_core::ds402::cyclic_pos_status_word_t*)status_word_->data();
        
          if( sw_->bits.internal_limit_active )
            ROS_WARN_THROTTLE(2,"[ %s%s%s%s ] Internal limit active ",  BOLDCYAN(), module_->getIdentifier().c_str(), RESET(),YELLOW() );
          
          if( sw_->bits.target_reached)
            ROS_WARN_THROTTLE(2,"[ %s%s%s%s ] Target reached",  BOLDCYAN(), module_->getIdentifier().c_str(), RESET(),YELLOW() );
          
          if( sw_->bits.following_error )
          {
            ret = coe_driver::CoeHwPlugin::DEVICE_ERROR;
            ROS_WARN_THROTTLE(2,"[ %s%s%s%s ] Tracking following error",  BOLDCYAN(), module_->getIdentifier().c_str(), RESET(),YELLOW() );
          }
          
          /*if( !sw_->bits.drive_follows_the_command )
          {
            ret = coe_driver::CoeHwPlugin::DEVICE_ERROR;
            ROS_WARN_THROTTLE(2,"[ %s%s%s%s ] The drive does not follow the command",  BOLDCYAN(), module_->getIdentifier().c_str(), RESET(),YELLOW() );
          }*/
        }
      }
      break;
    }
    
    state_pub_dec_++;
    if(state_pub_dec_ % 10 == 0)
    {
      if( state_pub_ && state_pub_->trylock() )
      {
        state_pub_->msg_.addresses.clear();
        state_pub_->msg_.statesid.clear();
        state_pub_->msg_.states.clear();
        state_pub_->msg_.motor_position.clear();
        state_pub_->msg_.motor_velocity.clear();
        state_pub_->msg_.motor_effort.clear();
        state_pub_->msg_.load_position.clear();
        state_pub_->msg_.load_velocity.clear();
        state_pub_->msg_.load_effort.clear();

        state_pub_->msg_.tracking_error_load_position.clear();
        state_pub_->msg_.tracking_error_load_velocity.clear();
        state_pub_->msg_.tracking_error_load_effort.  clear();

        state_pub_->msg_.addresses.push_back    ( 0 );
        state_pub_->msg_.statesid.push_back ( coe_core::ds402::to_string( stateid ) );      
        state_pub_->msg_.states.push_back( stateid );
        state_pub_->msg_.motor_position.push_back( position.entry->get<int32_t>( ) );
        state_pub_->msg_.motor_velocity.push_back( velocity.entry->get<int32_t>( ) );
        state_pub_->msg_.motor_effort. push_back ( torque.entry->get<int16_t>  ( ) );
        
        state_pub_->msg_.load_position.push_back( feedback_position_ );
        state_pub_->msg_.load_velocity.push_back( feedback_velocity_ );
        state_pub_->msg_.load_effort.  push_back( feedback_torque_   );

        state_pub_->msg_.tracking_error_load_position.push_back( command_position_ - feedback_position_ );
        state_pub_->msg_.tracking_error_load_velocity.push_back( command_velocity_ - feedback_velocity_ );
        state_pub_->msg_.tracking_error_load_effort.  push_back( command_effort_   - feedback_torque_   );

        state_pub_->unlockAndPublish();
      }
    }
  }
  catch( std::exception& e )
  {
    ROS_ERROR("exception caught in READ: \n%s\n",e.what());
    return CoeHwPlugin::EXCEPTION_ERROR;
  }
  
  if( checkErrorsActive( ) )
  {
    return CoeHwPlugin::DEVICE_ERROR;
  }
  
  return ret;
}



/*
 * 
 * 
 * 
 */
coe_driver::CoeHwPlugin::Error DS402Complete::write()
{
  CoeHwPlugin::Error ret = CoeHwPlugin::NONE_ERROR;
  try 
  {
    double nominal_operational_time = ( module_->getLoopRateDecimation() * operational_time_ );
    clock_gettime(CLOCK_MONOTONIC, &write_time_ );
    double actual_operational_time = realtime_utilities::timer_difference_s( &write_time_, &write_time_prev_ );
    write_time_prev_ = write_time_;
    if( actual_operational_time < 1e-4 ) // first cycle;
    {
      ROS_DEBUG("First Cycle");
      actual_operational_time = nominal_operational_time;
    }
    double command_position        = command_position_ ;
    double command_velocity_offset = operation_mode_    == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION ?  command_velocity_ : 0.0;
    double command_velocity        = operation_mode_    == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION ?               0.0  : command_velocity_  ;
    double command_effort          = 0.0;
    double command_effort_offset   = command_effort_;

    command_position = command_position < min_software_position_limit_ ? min_software_position_limit_
                     : command_position > max_software_position_limit_ ? max_software_position_limit_
                     : command_position;

    command_velocity = std::fabs( command_velocity ) > maximum_profile_velocity_
                     ? ( command_velocity > 0 ? 1 : -1 ) * maximum_profile_velocity_
                     : command_velocity;


    command_velocity_offset = std::fabs( command_velocity_offset ) > maximum_profile_velocity_
                     ? ( command_velocity_offset > 0 ? 1 : -1 ) * maximum_profile_velocity_
                     : command_velocity_offset;

    switch( operation_mode_ )
    {
      case coe_core::ds402::MOO_HOMING:
      {
        if( coe_core::ds402::to_stateid( status_word_->value() ) == coe_core::ds402::STATE_OPERATION_ENABLED) 
        {
          
          coe_core::ds402::homing_status_word_t*  sw_ = (coe_core::ds402::homing_status_word_t* )status_word_->data();
          coe_core::ds402::homing_control_word_t* cw_ = (coe_core::ds402::homing_control_word_t*)control_word_->data();
          if( sw_->bits.homing_error == 1 ) 
          {
            cw_->bits.home_operation_start = 0;
            cw_->bits.halt = 1;

            operation_mode_state_ = CoeHwPlugin::OPERATION_MODE_ERROR;
            ROS_ERROR( "**** **** ERROR IN HOMING FROM status word: %s ******", coe_core::ds402::to_string ( *sw_, 'b' ).c_str() );    
          } 
          else if( sw_->bits.homing_attained && sw_->bits.target_reached ) 
          {
            ROS_INFO_THROTTLE(5, "**** **** HOMING ATTAINED and TARGET REACHED status word: %s ******", coe_core::ds402::to_string ( *sw_, 'b' ).c_str() );    
            cw_->bits.home_operation_start = 0;
            cw_->bits.halt = 0;
            operation_mode_state_ = CoeHwPlugin::OPERATION_MODE_GOAL_ACHIEVED;
          } 
          else 
          {
            cw_->bits.home_operation_start = 1;
            cw_->bits.halt                 = 0;
          }
        }
      } 
      break;
      case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION:
      {
          // ----
        double delta_position_command = command_position - command_position_prev_ ;
        int    sign_delta_position_command = ( delta_position_command > 0 ? 1 : - 1);
        double abs_delta_position_command  = std::fabs( delta_position_command );
        if( (abs_delta_position_command / actual_operational_time) > maximum_profile_velocity_ )
        {
            delta_position_command = sign_delta_position_command * maximum_profile_velocity_ * actual_operational_time;
        }
        if( (abs_delta_position_command  /  std::pow(actual_operational_time,2) ) > profiled_acceleration_ )
        {
            delta_position_command = 0.5 * sign_delta_position_command * profiled_acceleration_ * std::pow(actual_operational_time,2);
        }
        command_position = command_position_prev_ + delta_position_command;

        // ----
        int    sign_velocity_command = ( command_velocity > 0 ? 1 : - 1);
        double abs_velocity_command  = std::fabs( command_velocity );
        if( abs_velocity_command  > maximum_profile_velocity_ )
        {
            command_velocity = sign_velocity_command * maximum_profile_velocity_;
        }
        if( ( abs_velocity_command  / actual_operational_time ) > profiled_acceleration_)
        {
            command_velocity = sign_velocity_command * profiled_acceleration_ * actual_operational_time;
        }

        coe_core::ds402::cyclic_pos_status_word_t* sw = (coe_core::ds402::cyclic_pos_status_word_t*)status_word_->data();
        if( coe_core::ds402::to_stateid( *(coe_core::ds402::status_word_t*)sw ) == coe_core::ds402::STATE_OPERATION_ENABLED)
        {
          coe_core::ds402::cyclic_pos_control_word_t*  cw = (coe_core::ds402::cyclic_pos_control_word_t*)control_word_->data();
          cw->bits.operation_specific = 1;
          cw->bits.switch_on = 1;
          cw->bits.enable_operation = 1;
          cw->bits.enable_voltage = 1;
          cw->bits.quick_stop = 1;
          cw->bits.halt = 0;
        }
        else
        {
          ROS_INFO_THROTTLE(2,"^^^^^^^^^^^^^ State operation NOT enabled (state: %s)", coe_core::ds402::to_string( *sw, 's' ).c_str() );
        }
      }
      break;

      case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY:
      {
        if( coe_core::ds402::to_stateid( status_word_->value() ) == coe_core::ds402::STATE_OPERATION_ENABLED)
        {
          coe_core::ds402::cyclic_pos_control_word_t*  cw_ = (coe_core::ds402::cyclic_pos_control_word_t*)control_word_->data();
          cw_->bits.operation_specific = 1;
          cw_->bits.switch_on = 1;
          cw_->bits.enable_operation = 1;
          cw_->bits.enable_voltage = 1;
          cw_->bits.quick_stop = 1;
          cw_->bits.halt = 0;
        }
      }
      break;
    }
    if( do_control_word_override_ )
        setControlWord( control_word_override_ );

    // -------------
    auto & position =  module_->getAxisCommand( RxPdoMandatory::Key[RxPdoMandatory::POSITION ] );
    position.entry->set<int32_t>( ( ( command_position * position.gear_ratio ) / ( 2*M_PI ) ) * position.counter_per_motor_round );

    auto & target_velocity =  module_->getAxisCommand( RxPdoMandatory::Key[RxPdoMandatory::VELOCITY ] );
    target_velocity.entry->set<int32_t>( ( ( command_velocity * target_velocity.gear_ratio ) / ( 2*M_PI ) ) * target_velocity.counter_per_motor_round );

    auto & offset_velocity =  module_->getAxisCommand( "velocity_offset"  );
    offset_velocity.entry->set<int32_t>( ( ( command_velocity_offset * offset_velocity.gear_ratio ) / ( 2*M_PI ) ) * offset_velocity.counter_per_motor_round );

    auto & target_torque =  module_->getAxisCommand( RxPdoMandatory::Key[RxPdoMandatory::TORQUE ] );
    target_torque.entry->set<int16_t>( ( ( command_effort / target_torque.gear_ratio ) / target_torque.nominal_motor_torque ) * 1000.0 );

    auto & offset_torque =  module_->getAxisCommand( "torque_offset" );
    offset_torque.entry->set<int16_t>( ( ( command_effort_offset / offset_torque.gear_ratio ) / offset_torque.nominal_motor_torque ) * 1000.0 );


    for( auto & a_i  :  module_->getAnalogOutputs(  ) )
    {
      a_i.second.entry->set<int32_t>( ( analog_outputs_[ a_i.first ] - a_i.second.offset ) / a_i.second.scale );
    }

    if( module_->getDigitalOutputs().size() )
    {
      std::bitset< 32 > bits( 0x0 );
      for( auto & d_i  :  module_->getDigitalOutputs(  ) )
      {
        bits[ d_i.second.bit ] = digital_outputs_[ d_i.first ];
      }
      module_->getDigitalOutputs().begin()->second.entry->set<uint32_t>( bits.to_ulong() ) ;
    }
    // -------------
    
    command_pub_dec_cnt_++;
    if(command_pub_ && ( command_pub_dec_cnt_ % command_pub_dec_ == 0 ))
    {
      if( command_pub_->trylock() )
      {
        command_pub_->msg_.addresses.clear();
        command_pub_->msg_.statesid.clear();
        command_pub_->msg_.states.clear();
        command_pub_->msg_.motor_position.clear();
        command_pub_->msg_.motor_velocity.clear();
        command_pub_->msg_.motor_effort.clear();

        command_pub_->msg_.load_received_position.clear();
        command_pub_->msg_.load_saturated_position.clear();
        command_pub_->msg_.load_delta_position.clear();
        command_pub_->msg_.load_received_velocity.clear();
        command_pub_->msg_.load_saturated_velocity.clear();
        command_pub_->msg_.load_delta_velocity.clear();
        command_pub_->msg_.load_received_effort.clear();
        command_pub_->msg_.load_saturated_effort.clear();
        command_pub_->msg_.load_delta_effort.clear();

        command_pub_->msg_.addresses.push_back    ( 0 );
        command_pub_->msg_.statesid.push_back  (  coe_core::ds402::to_string( statusid_prev_ ) );
        
        command_pub_->msg_.states.push_back( statusid_prev_ );
        command_pub_->msg_.motor_position.push_back( position.entry->get<int32_t>( ) );
        command_pub_->msg_.motor_velocity.push_back( target_velocity.entry->get<int32_t>( ) );
        command_pub_->msg_.motor_velocity.push_back( offset_velocity.entry->get<int32_t>( ) );
        command_pub_->msg_.motor_effort  .push_back ( target_torque.entry->get<int16_t>  ( ) );
        command_pub_->msg_.motor_effort  .push_back ( offset_torque.entry->get<int16_t>  ( ) );
        
        command_pub_->msg_.load_received_position   .push_back( command_position_ );;
        command_pub_->msg_.load_saturated_position  .push_back( command_position );;
        command_pub_->msg_.load_delta_position      .push_back( command_position - command_position_prev_ );

        command_pub_->msg_.load_received_velocity   .push_back( command_velocity_ );
        command_pub_->msg_.load_saturated_velocity  .push_back( command_velocity );
        command_pub_->msg_.load_delta_velocity      .push_back( command_velocity - command_velocity_prev_ );

        command_pub_->msg_.load_received_effort     .push_back( command_effort_ );
        command_pub_->msg_.load_saturated_effort    .push_back( command_effort );
        command_pub_->msg_.load_delta_effort        .push_back( command_effort - command_effort_prev_ );

        command_pub_->unlockAndPublish();
      }
    }

    command_position_prev_ = command_position;
    command_velocity_prev_ = command_velocity;
    command_effort_prev_   = command_effort;
    motor_command_position_prev_ = position.entry->get<int32_t>();


    ret = CoeHwPlugin::write();
  }
  catch( std::exception& e )
  {
    ROS_ERROR("exception caught in READ: \n%s\n",e.what());
    return CoeHwPlugin::EXCEPTION_ERROR;
  }
  
  return ret;
}

///////////////////////////////////////
PLUGINLIB_EXPORT_CLASS(coe_hw_plugins::DS402Complete, coe_driver::CoeHwPlugin );


}
