#include <realtime_tools/realtime_publisher.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>

#include <coe_core/coe_base.h>
#include <coe_core/ds301/coe_bitwise_struct.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>
#include <coe_core/ds402/coe_bitwise_struct.h>

#include <coe_driver/diagnostic/coe_generic_analyzer.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>


namespace coe_hw_plugins
{

class DOCompact : public ::coe_driver::CoeHwPlugin
{
private:

  std::map< std::string, bool >   digital_outputs_;
  void getDeviceErrors ( std::vector< std::pair< std::string,std::string> >& errors_map );
  bool  checkErrorsActive     ( ) { return false; }
  
public:

  DOCompact( ) { }
  
  bool initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address );
  coe_driver::CoeHwPlugin::Error read();
  coe_driver::CoeHwPlugin::Error write();
  
  bool                      setTargetState        ( const std::string& state_name );
  std::vector<std::string>  getStateNames         ( ) const;
  std::string               getActualState        ( );
    
 ///////////////////////////////////////////////////////////
  std::vector<std::string>  getBytesInputNames   ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string>  getBytesOutputNames  ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string>  getDigitalInputNames ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string>  getDigitalOutputNames( ) const;
  std::vector<std::string>  getAnalogOutputNames ( ) const { throw std::runtime_error("Not supported for this implementation."); };  
  std::vector<std::string>  getAnalogInputNames  ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  
  void      jointStateHandle        (double** pos,  double** vel, double** eff) { throw std::runtime_error("Not supported for this implementation."); }
  void      jointCommandHandle      (double** pos,  double** vel, double** eff) { throw std::runtime_error("Not supported for this implementation."); }
  uint64_t* bytesInputValueHandle   (const std::string& name )                  { throw std::runtime_error("Not supported for this implementation."); }
  uint64_t* bytesOutputValueHandle  (const std::string& name )                  { throw std::runtime_error("Not supported for this implementation."); }
  double*   analogInputValueHandle  (const std::string& name )                  { throw std::runtime_error("Not supported for this implementation."); }
  double*   analogOutputValueHandle (const std::string& name )                  { throw std::runtime_error("Not supported for this implementation."); }
  bool*     digitalInputValueHandle (const std::string& name )                  { throw std::runtime_error("Not supported for this implementation."); }
  bool*     digitalOutputValueHandle(const std::string& name );
  

  bool hasBytesInputs       ( )  { return false; }
  bool hasBytesOutputs      ( )  { return false; }
  bool hasDigitalInputs     ( )  { return false; }
  bool hasDigitalOutputs    ( )  { return true;  }
  bool hasAnalogInputs      ( )  { return false; }
  bool hasAnalogOutputs     ( )  { return false; }
  bool isActuator           ( )  { return false; }
  
};




///////////////////////////////////////
bool DOCompact::initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address)
{

  if (!CoeHwPlugin::initialize(nh, device_coe_parameter, address) )
  return false;
  
  //Pointer to 
  for( auto const & d_o : module_->getDigitalOutputs  ( ) ) digital_outputs_  [ d_o.first ] = 0.0;
  
  // Init values
  read();     
  
  return true;
}


std::vector<std::string> DOCompact::getStateNames ( ) const 
{ 
  return std::vector<std::string>{ "Not supported" }; 
}
std::string  DOCompact::getActualState ( )  
{ 
  return "Not supported" ; 
}

std::vector<std::string> DOCompact::getDigitalOutputNames ( ) const 
{ 
  std::vector<std::string> n; 
  for( auto const & m : module_->getDigitalOutputs()   ) 
    n.push_back(m.first); 
  return n; 
}

bool*  DOCompact::digitalOutputValueHandle (const std::string& name) 
{
  return &( digital_outputs_[name] );
}

void DOCompact::getDeviceErrors ( std::vector< std::pair< std::string,std::string> >& errors_map )
{ 
  errors_map.clear();
  errors_map.push_back(std::make_pair( "N/A", "Not yet supported") ); 
}

bool DOCompact::setTargetState( const std::string& state_name )
{
  return true;
}



coe_driver::CoeHwPlugin::Error DOCompact::read()
{
  return CoeHwPlugin::read();
  
}
coe_driver::CoeHwPlugin::Error DOCompact::write()
{
  for( auto & d_o  :  module_->getDigitalOutputs(  ) )
  { 
    d_o.second.entry->set<bool>( digital_outputs_[ d_o.first ] );
  }
  
  return CoeHwPlugin::write();
  
}



///////////////////////////////////////
PLUGINLIB_EXPORT_CLASS(coe_hw_plugins::DOCompact, coe_driver::CoeHwPlugin );


}
