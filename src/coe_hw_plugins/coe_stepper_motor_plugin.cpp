#include <realtime_tools/realtime_publisher.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>

#include <coe_core/coe_base.h>
#include <coe_core/ds301/coe_bitwise_struct.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>
#include <coe_core/ds402/coe_bitwise_struct.h>

#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>


namespace coe_hw_plugins
{


  
class StepperMotor : public coe_driver::CoeHwPlugin
{
private:

  std::map< std::string, bool >  do_;
  std::map< std::string, bool >  di_;
  double                         command_velocity_;
  double                         position_;
  double                         velocity_;
  double                         torque_;
  std::vector< std::string >     state_names_;


  void getDeviceErrors ( std::vector< std::pair< std::string,std::string> >& errors_map )
  { 
    errors_map.clear();
    errors_map.push_back(std::make_pair( "N/A", "Not yet supported") ); 
  }
  
  bool  checkErrorsActive     ( ) { return false; }
    
public:

  StepperMotor( ) { }
  
  bool initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address );
  coe_driver::CoeHwPlugin::Error read();
  coe_driver::CoeHwPlugin::Error write();
  
  std::vector<std::string> getStateNames         ( ) const  { return state_names_; }

  bool                     setTargetState        ( const std::string& state_name );
  std::string              getActualState        ( ) { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string> getBytesInputNames    ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string> getBytesOutputNames   ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string> getAnalogInputNames   ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string> getAnalogOutputNames  ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  
  std::vector<std::string> getDigitalInputNames  ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string> getDigitalOutputNames ( ) const { throw std::runtime_error("Not supported for this implementation."); }

  void jointStateHandle   ( double** pos, double** vel, double** eff );
  void jointCommandHandle ( double** pos, double** vel, double** eff );
  
  uint64_t* bytesInputValueHandle  (const std::string& name ){ throw std::runtime_error("Not supported for this implementation."); }
  uint64_t* bytesOutputValueHandle (const std::string& name ){ throw std::runtime_error("Not supported for this implementation."); }
  double* analogInputValueHandle  ( const std::string& name )  { throw std::runtime_error("Not supported for this implementation."); }
  double* analogOutputValueHandle ( const std::string& name )  { throw std::runtime_error("Not supported for this implementation."); }
  bool*   digitalInputValueHandle ( const std::string& name )  { throw std::runtime_error("Not supported for this implementation."); }
  bool*   digitalOutputValueHandle( const std::string& name )  { throw std::runtime_error("Not supported for this implementation."); }

  bool hasBytesInputs    ( ) { return true;  }
  bool hasBytesOutputs   ( ) { return true;  }
  bool hasDigitalInputs  ( ) { return false; }
  bool hasDigitalOutputs ( ) { return false; }
  bool hasAnalogInputs   ( ) { return true;  }
  bool hasAnalogOutputs  ( ) { return false; }
  bool isActuator        ( ) { return false; }
  
};


bool StepperMotor::initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address )
{
  if (!CoeHwPlugin::initialize(nh, device_coe_parameter, address) )
    return false;
  
  state_names_.clear();
  state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_NO_MODE   ) );
  state_names_.push_back( coe_core::ds402::MODEOPERATIONSID_STRINGS.at( coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY   ) );

  //Pointer to 
  do_["Enable"] = false;
  do_["Reset"]  = false;
  do_["Reduce Torque"] = false;    
  
  di_["Ready To Enable" ] = false;
  di_["Warning"         ] = false;
  di_["Ready"           ] = false;
  di_["Moving Positive" ] = false;
  di_["Moving Negative" ] = false;
  di_["Torque Reduced"  ] = false;
  di_["DI1"             ] = false;
  di_["DI2"             ] = false;
  di_["Sync Error"      ] = false;
  di_["TxPDOToggle"     ] = false;

  command_velocity_   = 0.0;
  position_=std::nan("1");
  velocity_=command_velocity_;
  torque_=std::nan("1");
  
  return true;
}



bool StepperMotor::setTargetState( const std::string& state_name )
{
  coe_core::ds402::ModeOperationID moo = coe_core::ds402::to_modeofoperationid(state_name);
  switch( moo )
  {
    case coe_core::ds402::MOO_NO_MODE : 
      do_.at("Enable") = false;
      do_.at("Reset") = false;
      do_.at("Reduce Torque") = false;
      break;
      
    case coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY  : 
      do_.at("Enable") = true;
      do_.at("Reset") = false;
      do_.at("Reduce Torque") = false;
      break;
    default:
      return false;
  }
  return true;
}


coe_driver::CoeHwPlugin::Error StepperMotor::read()
{
  coe_driver::CoeHwPlugin::Error ret  = CoeHwPlugin::read();
  if( ret != coe_driver::CoeHwPlugin::NONE_ERROR )
    return ret;
  
  di_["Ready To Enable" ] = (*(module_->getTxPdo().find(0x1a03,1)))->get<bool>();
  di_["Ready"           ] = (*(module_->getTxPdo().find(0x1a03,2)))->get<bool>();
  di_["Warning"         ] = (*(module_->getTxPdo().find(0x1a03,3)))->get<bool>();
  di_["Error"           ] = (*(module_->getTxPdo().find(0x1a03,4)))->get<bool>();
  
  di_["Moving Positive" ] = (*(module_->getTxPdo().find(0x1a03,5)))->get<bool>();
  di_["Moving Negative" ] = (*(module_->getTxPdo().find(0x1a03,6)))->get<bool>();
  di_["Torque Reduced"  ] = (*(module_->getTxPdo().find(0x1a03,7)))->get<bool>();
  
  di_["DI1"             ] = (*(module_->getTxPdo().find(0x1a03,10)))->get<bool>();
  di_["DI2"             ] = (*(module_->getTxPdo().find(0x1a03,11)))->get<bool>();
  di_["Sync Error"      ] = (*(module_->getTxPdo().find(0x1a03,12)))->get<bool>();
  di_["TxPDOToggle"     ] = (*(module_->getTxPdo().find(0x1a03,14)))->get<bool>();
  
  return coe_driver::CoeHwPlugin::NONE_ERROR;
  
}
coe_driver::CoeHwPlugin::Error StepperMotor::write()
{
  auto & velocity =  module_->getAxisCommand( "velocity");
  assert( 0 ); //velocity.entry->set<int16_t>( ( command_velocity_ -  velocity.offset ) / velocity.scale );

  (*(module_->getRxPdo().find(0x1602,1)))->set<bool>( do_["Enable"]        );
  (*(module_->getRxPdo().find(0x1602,2)))->set<bool>( do_["Reset"]         );
  (*(module_->getRxPdo().find(0x1602,3)))->set<bool>( do_["Reduce Torque"] );
  
  return CoeHwPlugin::write();
  
}


void StepperMotor::jointStateHandle ( double** pos, double** vel, double** eff ) {
  *pos = &position_;
  *vel = &command_velocity_;
  *eff = &torque_;
}

void StepperMotor::jointCommandHandle ( double** pos, double** vel, double** eff )
{
  *vel = &command_velocity_;
}


///////////////////////////////////////

PLUGINLIB_EXPORT_CLASS(coe_hw_plugins::StepperMotor, coe_driver::CoeHwPlugin );


}
