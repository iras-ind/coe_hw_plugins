#ifndef __COE_DS402_PLUNGIN_HEADER__H__
#define __COE_DS402_PLUNGIN_HEADER__H__

#include <realtime_tools/realtime_publisher.h>
#include <pluginlib/class_list_macros.h>
#include <std_msgs/Float64MultiArray.h>

 
#include <coe_core/coe_base.h>
#include <coe_core/ds301/coe_bitwise_struct.h>
#include <coe_core/ds301/coe_sdo_dictionary.h>
#include <coe_core/ds402/coe_xfsm_utilities.h>
#include <coe_core/ds402/coe_xfsm_symbols.h>
#include <coe_core/ds402/coe_bitwise_struct.h>

#include <coe_driver/diagnostic/coe_generic_analyzer.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>
#include <coe_driver/Ds402Command.h>
#include <coe_driver/Ds402States.h>

namespace coe_hw_plugins
{

static const std::string DIAGNOSTIC_MSG_NS = "/diagnostic";
  
namespace TxPdoMandatory
{
  const char*      Key         [ 4 ] = { "position", "velocity", "torque", "status_word"   };
  const uint16_t   Index       [ 4 ] = {     0x1A0E,     0x1A0F,   0x1A12,       0x1A0A    };
  enum             ID                  { POSITION=0,   VELOCITY,   TORQUE,   STATUS_WORD   };
}

namespace RxPdoMandatory
{
  const char*      Key         [ 4 ] = { "position", "velocity", "torque", "control_word"  };
  const uint16_t   Index       [ 4 ] = {     0x160F,     0x161C,   0x160C,         0x160A  };
  enum             ID                  { POSITION=0,   VELOCITY,   TORQUE,   CONTROL_WORD  };
}


class DS402Complete : public coe_driver::CoeHwPlugin
{
protected:

  double min_position_range_limit_;
  double max_position_range_limit_;
  double min_software_position_limit_;
  double max_software_position_limit_;
  double maximum_profile_velocity_;
  double profiled_acceleration_;
  
  int       state_pub_dec_;
  int       command_pub_dec_;
  uint64_t  state_pub_dec_cnt_;
  uint64_t  command_pub_dec_cnt_;
  std::shared_ptr< realtime_tools::RealtimePublisher< coe_driver::Ds402States > > state_pub_;
  std::shared_ptr< realtime_tools::RealtimePublisher< coe_driver::Ds402Command > > command_pub_;

  std::mutex                           read_mtx_;
  double                               feedback_position_;
  double                               feedback_velocity_;
  double                               feedback_torque_;
  
  double                               command_position_;
  double                               command_velocity_;
  double                               command_effort_;

  bool                                 do_control_word_override_;
  coe_core::ds402::control_word_t      control_word_override_;
  struct timespec                      write_time_;
  struct timespec                      write_time_prev_;
  double                               command_position_prev_;
  int32_t                              motor_command_position_prev_;
  double                               command_velocity_prev_;
  double                               command_effort_prev_;


  std::map< std::string, double >      analog_inputs_;
  std::map< std::string, double >      analog_outputs_;

  std::map< std::string, bool >        digital_inputs_;
  std::map< std::string, bool >        digital_outputs_;

  coe_core::DataObjectEntry<uint16_t>* status_word_;
  coe_core::DataObjectEntry<uint16_t>* control_word_;
  std::vector< std::string >           state_names_;
  
  
  bool                                 switching_operation_mode_;
  coe_core::ds402::StateID             statusid_prev_;
  coe_core::ds402::ModeOperationID     operation_mode_;
  
  coe_core::ds402::ModeOperationID     getOperationMode  ( );
  bool                                 setOperationMode  ( const coe_core::ds402::ModeOperationID moo, const uint8_t max_num_trials = 250 );
  coe_core::ds402::ModeOperationID     getOperationModeID( );
  
  
  bool  checkErrorsActive( );
  
public:

  DS402Complete( );
  virtual ~DS402Complete( );
  
  void getDeviceErrors ( std::vector< std::pair< std::string,std::string> >& errors_map );
  
  bool initialize(ros::NodeHandle& nh, const std::string& device_coe_parameter, int address);
  coe_driver::CoeHwPlugin::Error read();
  coe_driver::CoeHwPlugin::Error write();
  
  std::vector<std::string>  getStateNames         ( ) const;
  std::string               getActualState        ( );
  
  //---------------------------------------------------------------------------
  std::vector<std::string>  getBytesInputNames   ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  std::vector<std::string>  getBytesOutputNames  ( ) const { throw std::runtime_error("Not supported for this implementation."); }
  
  std::vector<std::string>  getAnalogInputNames   ( ) const;
  std::vector<std::string>  getAnalogOutputNames  ( ) const;
  
  std::vector<std::string>  getDigitalInputNames  ( ) const;
  std::vector<std::string>  getDigitalOutputNames ( ) const;

  bool  setTargetState            ( const std::string& state_name );
  bool  setControlWord            ( const coe_core::ds402::control_word_t& control_word );
  

  void  jointStateHandle          ( double** pos, double** vel, double** eff );
  void  jointCommandHandle        ( double** pos, double** vel, double** eff );
  
  uint64_t* bytesInputValueHandle (const std::string& name ) { throw std::runtime_error("Not supported for this implementation."); }
  uint64_t* bytesOutputValueHandle(const std::string& name ) { throw std::runtime_error("Not supported for this implementation."); }
  
  double* analogInputValueHandle  ( const std::string& name );
  double* analogOutputValueHandle ( const std::string& name );
  
  bool* digitalInputValueHandle   ( const std::string& name );
  bool* digitalOutputValueHandle  ( const std::string& name );
  
  bool hasBytesInputs     ( ) { return false; }
  bool hasBytesOutputs    ( ) { return false; }
  bool hasAnalogInputs    ( ) { return analog_inputs_.size();    };
  bool hasAnalogOutputs   ( ) { return analog_outputs_.size();   };
  bool hasDigitalInputs   ( ) { return digital_inputs_.size();   };
  bool hasDigitalOutputs  ( ) { return digital_outputs_.size();  };
  bool isActuator         ( ) { return true;                     };
  
  //---------------------------------------------------------------------------
  coe_core::ds402::status_word_t   getStatusWord         ( );
  coe_core::ds402::control_word_t  getControlWord        ( );
  uint16_t*                        stateHandle           ( );
  uint16_t*                        controlWordHandle     ( );
  //---------------------------------------------------------------------------
 
  

};





}

#endif
